# Start server #
```
#!bash
docker-compose up -d
```

# Login to host #
```
#!bash
docker login docker.validname.com
admin
admin
```

# Push new images #
```
#!bash
docker pull alpine
docker tag alpine docker.validname.com/alpine
docker push docker.validname.com/alpine
```

# Howto create htpasswd #
docker run --entrypoint htpasswd registry:2 -Bbn testuser testpassword > htpasswd